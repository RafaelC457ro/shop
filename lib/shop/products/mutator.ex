defmodule Shop.Products.Mutator do
  @moduledoc """
  Mutate and persist data in database
  """
  alias Shop.Products.Product

  defp create_product(attrs) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end
end
