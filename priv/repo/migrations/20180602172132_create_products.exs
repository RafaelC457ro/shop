defmodule Shop.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :category, :string
      add :code, :string
      add :description, :string
      add :price, :float

      timestamps()
    end

  end
end
