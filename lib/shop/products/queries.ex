defmodule Shop.Products.Queries do
  @moduledoc """
  Queries Products from database
  """
  import Ecto.Query, warn: false
  alias Shop.Products.Product

  def find_product(id) do
    from p in Product, where: id = ^id
  end

  def find_all, do: Product
end
