defmodule Shop.Products.Loader do
  @moduledoc """
  Database Loader of Modules
  """
  alias Shop.Repo
  alias Shop.Products.Queries

  def get_product(id) do
    id
    |> Queries.find_product()
    |> Repo.one()
  end

  def list_products do
    Queries.find_all()
    |> Repo.all()
  end
end
